import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';
import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http : HttpClient) { }

  userdataUrl = "../assets/users.json";

  getUsers(): Observable<any>{
    return this.http.get<User>(this.userdataUrl);
  }

  saveUser(user){
    console.log(user);
    const blob = new Blob([JSON.stringify(user)], {type : 'application/json'});
    console.log(blob);
    saveAs(blob, this.userdataUrl);
  }


}
