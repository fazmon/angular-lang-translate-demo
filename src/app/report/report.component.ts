import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { User } from '../user';
import { TranslateService } from '@ngx-translate/core';
import { EmailValidatiors } from './email.validators';
import { Observable, Subject } from "rxjs";


@Component({
  selector: 'report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  users: User;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private dataService: DataService, private translate: TranslateService) {
    translate.addLangs(["en", "sp"]);
    translate.setDefaultLang('en');
    translate.use('en');
  }

  ngOnInit() {
    this.dtOptions = {
      pageLength: 6,
      stateSave: true,
      lengthMenu: [[5, 10, 20, -1], [5, 10, 20, "All"]],
      processing: true
    };

    this.dataService.getUsers()
      .subscribe(response => {
        this.users = response;
        this.dtTrigger.next();
      })
  }

  form = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, EmailValidatiors.noSpace]),
      company: new FormControl('', Validators.required)
  });

  get name() {
    return this.form.get('name');
  }

  get email() {
    return this.form.get('email');
  }

  get company() {
    return this.form.get('company');
  }

  saveUser(user: User) {
    this.dataService.saveUser(JSON.stringify(user));
  }

  selectLang(event: any) {
    this.translate.use(event.target.value);
  }

}
